import { useState } from "react";
import PropTypes from "prop-types";

import { BsArrowUpCircleFill } from "react-icons/bs";
import { BsArrowDownCircleFill } from "react-icons/bs";

const Questions = ({ title, info }) => {
    const [showInfo, setShowInfo] = useState(false);

    return (
        <div className="question">
            <header>
                <h4>{title}</h4>

                <button className="btn" onClick={() => setShowInfo(!showInfo)}>
                    {showInfo ? <BsArrowUpCircleFill /> : <BsArrowDownCircleFill />}
                </button>
            </header>
            {showInfo && <p>{info}</p>}
        </div>
    );
};

Questions.propTypes = {
    title: PropTypes.string.isRequired,
    info: PropTypes.string.isRequired
};

export default Questions;
