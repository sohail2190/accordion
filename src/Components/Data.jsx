


const question = [
    {
      id: 1,
      title: "What is JavaScript?",
      info: "JavaScript is a programming language that enables interactive web pages. It is commonly used for client-side web development."
    },
    {
      id: 2,
      title: "What are the different data types in JavaScript?",
      info: "JavaScript supports various data types including string, number, boolean, null, undefined, object, and symbol."
    },
    {
      id: 3,
      title: "What is the difference between '==' and '===' operators in JavaScript?",
      info: "The '==' operator checks for equality of value, while the '===' operator checks for equality of value and type."
    },
    {
      id: 4,
      title: "What is the DOM (Document Object Model) in JavaScript?",
      info: "The DOM is a programming interface for web documents. It represents the structure of a document as a tree of objects, allowing JavaScript to interact with and modify the content and style of web pages."
    },
    {
      id: 5,
      title: "What are closures in JavaScript?",
      info: "A closure is a function that has access to its own scope, as well as the scope in which it was defined. Closures are commonly used to create private variables and functions in JavaScript."
    },
    {
      id: 6,
      title: "What are higher-order functions in JavaScript?",
      info: "Higher-order functions are functions that take other functions as arguments or return functions as results. They enable a functional programming style in JavaScript."
    },
    {
      id: 7,
      title: "How do you handle asynchronous operations in JavaScript?",
      info: "Asynchronous operations in JavaScript can be handled using callbacks, promises, or async/await syntax. These mechanisms allow you to execute code asynchronously and handle the results once they become available."
    },
    {
      id: 8,
      title: "What are some common array methods in JavaScript?",
      info: "JavaScript provides several built-in array methods such as push(), pop(), shift(), unshift(), slice(), splice(), map(), filter(), reduce(), forEach(), and more. These methods allow you to manipulate arrays in various ways."
    },
    {
      id: 9,
      title: "How do you handle errors in JavaScript?",
      info: "Errors in JavaScript can be handled using try...catch statements. You can wrap code that might throw an error inside a try block, and use the catch block to handle any errors that occur."
    },
    {
      id: 10,
      title: "What is the difference between 'var', 'let', and 'const' in JavaScript?",
      info: "'var' is function-scoped, 'let' is block-scoped, and 'const' is also block-scoped but cannot be reassigned. 'let' and 'const' were introduced in ES6 (ECMAScript 2015) and are preferred over 'var' for variable declaration."
    },
    {
        id: 11,
        title: "What is event bubbling and event capturing in JavaScript?",
        info: "Event bubbling is the process where the event starts from the target element and bubbles up through its ancestors. Event capturing is the opposite, where the event starts from the outermost ancestor and trickles down to the target element."
    },
    {
        id: 12,
        title: "What is the 'this' keyword in JavaScript?",
        info: "The 'this' keyword refers to the object it belongs to. In most cases, 'this' refers to the object which calls the function."
    },
    {
        id: 13,
        title: "What are the different types of loops in JavaScript?",
        info: "JavaScript supports several types of loops, including 'for' loops, 'while' loops, 'do-while' loops, and 'for...in' loops."
    },
    {
        id: 14,
        title: "What is scope in JavaScript?",
        info: "Scope defines the accessibility of variables and functions in a particular part of your code. JavaScript has two types of scope: global scope and local scope."
    },
    {
        id: 15,
        title: "What is the difference between null and undefined in JavaScript?",
        info: "Null represents the intentional absence of any object value, while undefined represents the absence of a defined value."
    },
    {
        id: 16,
        title: "What are the different ways to create objects in JavaScript?",
        info: "Objects in JavaScript can be created using object literals, the 'new' keyword, constructor functions, and ES6 classes."
    },
    {
        id: 17,
        title: "What is event delegation in JavaScript?",
        info: "Event delegation is a technique where you attach a single event listener to a parent element instead of multiple event listeners to individual child elements. This is particularly useful when you have a large number of dynamically created elements."
    },
    {
        id: 18,
        title: "What is a callback function?",
        info: "A callback function is a function that is passed as an argument to another function and is executed after some operation has been completed. Callback functions are commonly used in asynchronous programming."
    },
    {
        id: 19,
        title: "What is hoisting in JavaScript?",
        info: "Hoisting is a JavaScript mechanism where variables and function declarations are moved to the top of their containing scope during the compilation phase. However, only declarations are hoisted, not initializations."
    },
    {
        id: 20,
        title: "What is the difference between 'null' and 'undefined'?",
        info: "Null is an assignment value. It can be assigned to a variable as a representation of no value. Undefined means a variable has been declared but has not yet been assigned a value."
    }
  ];
  
  export default question;
  