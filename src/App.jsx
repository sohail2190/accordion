import React,{ Component } from "react";
import question from './Components/Data'
import Questions from "./Components/Questions";
import './App.css'


class App extends React.Component{

  render(){

    return(
      <>
      <div className="container">
        <h1>FAQ'S</h1>
        <section className="info">
          {
            question.map( (q)=>(
              <Questions key={q.id} {...q}/>
            ))
          }
        </section>
      </div>
      </>
    )
  }
}
export default App;
